****** DOCUMENTATION ******
For most up-to-date documentation visit http://www.dynamicwaterphysics.com.
A local copy is available in the Documentation directory.

****** UPGRADING ******
When upgrading between major versions (e.g. 2.1 to 2.2) please check upgrade notes. 
They are available in the Documentation directory or on the website above.

****** BURST ******
To enable Burst compilation install Burst from Unity Package Manager and
add "DWP_BURST" to Scripting Define Symbols under Project Settigns > Player.
Note that Burst does not support cross-compilation in some cases (e.g. Windows to Mac OS).