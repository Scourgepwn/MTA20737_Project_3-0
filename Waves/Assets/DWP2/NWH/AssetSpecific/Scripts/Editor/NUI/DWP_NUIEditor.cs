﻿namespace NWH.NUI
{
    /// <summary>
    /// NWH Vehicle Physics specific NUI Editor.
    /// </summary>
    public class DWP_NUIEditor : NUIEditor
    {
        public override string GetDocumentationBaseURL()
        {
            return "http://localhost/doku.php/";
        }
    }
}